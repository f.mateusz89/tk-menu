import { Action, combineReducers } from 'redux';

import { CLOSE_MENU, OPEN_MENU } from './constants';

function open(state = false, { type }: Action): boolean {
  switch (type) {
    case CLOSE_MENU:
      return false;

    case OPEN_MENU:
      return true;

    default:
      return state;
  }
}

export default combineReducers({
  open,
});
