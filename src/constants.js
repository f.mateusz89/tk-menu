// import createType from 'utils/create-type';
// TODO: Create module + import
export default function createType(name, prefix) {
    return prefix ? "[" + prefix + "] " + name : name;
}
var PREFIX = 'menu';
export var CLOSE_MENU = createType('CLOSE_MENU', PREFIX);
export var OPEN_MENU = createType('OPEN_MENU', PREFIX);
