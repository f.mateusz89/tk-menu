import { createSelector } from 'reselect';

import { State } from './types';

function rootSelector(state: State) {
  return state.menu;
}

export const open = createSelector(rootSelector, (root) => root.open);
