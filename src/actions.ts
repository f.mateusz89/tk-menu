import { CLOSE_MENU, OPEN_MENU } from './constants';

export const closeMenu = () => ({ type: CLOSE_MENU });

export const openMenu = () => ({ type: OPEN_MENU });
