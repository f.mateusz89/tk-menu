import { DrawerProps } from '@material-ui/core/Drawer';

export interface OwnProps {
  anchor?: DrawerProps['anchor'];
  className?: string;
  items: Item[];
}

export interface State {
  menu: {
    open: boolean;
  };
}

export interface Item {
  hideAfterClick?: boolean;
  icon?: JSX.Element;
  name: string;
  onClick?: () => void;
}
