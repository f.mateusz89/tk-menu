declare function actionCreator(actionName: string): import("@storybook/addon-actions").HandlerFunction;
export { actionCreator as action };
