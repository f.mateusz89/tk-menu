import { action as brokenAction } from '@storybook/addon-actions';
var action = function (name, options) {
    var constructedAction = brokenAction(name, options);
    return function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return constructedAction.apply(void 0, args.map(function (arg) {
            return typeof arg.persist === 'function' ? 'react-event' : arg;
        }));
    };
};
function actionCreator(actionName) {
    var actionWrapper = action(actionName);
    actionWrapper.toString = function toString() {
        return "function " + actionName + "() { }";
    };
    return actionWrapper;
}
export { actionCreator as action };
