/// <reference types="react" />
declare const withActions: (...actions: Function[]) => (story: Function) => JSX.Element;
export default withActions;
