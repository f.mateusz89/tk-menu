import React from 'react';
import { combineReducers, createStore } from 'redux';
import { Provider } from 'react-redux';
import merge from 'lodash/merge';

import { action } from '../actions';
import filledState from './filledState';
import menu from '../../reducer';

const store = createStore(
  combineReducers({
    menu,
  }),
);

const withStore = (...states) => (story) => {
  return (
    <Provider
      store={{
        ...store,
        dispatch: (dispatchAction) => {
          action(`[DISPATCH]${dispatchAction.type}`)(dispatchAction);

          return store.dispatch(dispatchAction);
        },
        getState() {
          const state = store.getState();

          return merge({}, ...states, state);
        },
      }}
    >
      {story()}
    </Provider>
  );
};

export { filledState, withStore };
