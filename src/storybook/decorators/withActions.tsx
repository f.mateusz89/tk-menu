import React, { Dispatch } from 'react';
import { Action } from 'redux';
import { connect } from 'react-redux';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    buttons: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },
  }),
);

const withActions = (...actions: Function[]) => (story: Function) => {
  function WithActions({ dispatch }: { dispatch: Dispatch<Action> }) {
    const classes = useStyles();

    return (
      <>
        <Box className={classes.buttons} p={1}>
          {actions.map((action, index) => (
            <Button
              key={index}
              color="primary"
              onClick={() => dispatch(action())}
              variant="contained"
            >
              {action.name}
            </Button>
          ))}
        </Box>

        {story()}
      </>
    );
  }

  const WithDispatch = connect(null, (dispatch) => ({ dispatch }))(WithActions);

  return <WithDispatch />;
};

export default withActions;
