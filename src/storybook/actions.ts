import { action as brokenAction } from '@storybook/addon-actions';

const action: typeof brokenAction = (name, options) => {
  const constructedAction = brokenAction(name, options);

  return (...args) => {
    return constructedAction(
      ...args.map((arg) =>
        typeof arg.persist === 'function' ? 'react-event' : arg,
      ),
    );
  };
};

function actionCreator(actionName: string) {
  const actionWrapper = action(actionName);

  actionWrapper.toString = function toString() {
    return `function ${actionName}() { }`;
  };

  return actionWrapper;
}

export { actionCreator as action };
