import { CLOSE_MENU, OPEN_MENU } from './constants';
export var closeMenu = function () { return ({ type: CLOSE_MENU }); };
export var openMenu = function () { return ({ type: OPEN_MENU }); };
