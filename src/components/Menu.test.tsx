import React from 'react';
import { shallow } from 'enzyme';

import { initLanguages } from 'language';
import { Item } from '../types';
import * as selectors from '../selectors';
import Menu from './Menu';

jest.mock('react-redux', () => ({
  useSelector: jest.fn((fn) => fn()),
  useDispatch: jest.fn(() => (action: any): any => action),
}));

function setup({ open }: any): void {
  jest.spyOn(selectors, 'open').mockReturnValue(open);
}

describe('[menu] Menu', () => {
  let items: Item[];

  beforeAll(() => {
    initLanguages();
  });

  beforeEach(() => {
    items = [
      { name: 'menu.important' },
      { name: 'menu.today' },
      { name: 'menu.settings' },
    ];
  });

  it('renders Menu', () => {
    setup({ open: true });
    const wrapper = shallow(<Menu items={items} />);

    expect(wrapper).toMatchSnapshot();
  });

  it('renders render hidden Menu', () => {
    setup({ open: false });
    const wrapper = shallow(<Menu items={items} />);

    expect(wrapper).toMatchSnapshot();
  });
});
