import React, { memo, useCallback } from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Toolbar from '@material-ui/core/Toolbar';

import { closeMenu } from '../actions';
import { OwnProps } from '../types';
import * as selectors from '../selectors';

const useStyles = makeStyles(() =>
  createStyles({
    paper: {
      display: 'flex',
      justifyContent: 'flex-end',
    },
    toolbar: {
      justifyContent: 'flex-end',
    },
  }),
);

function Menu({
  anchor = 'right',
  className,
  items,
}: OwnProps): React.ReactElement {
  const classes = useStyles();
  const dispatch = useDispatch();
  const open = useSelector(selectors.open);
  const onClose = (): void => {
    dispatch(closeMenu());
  };

  return (
    <Drawer
      anchor={anchor}
      classes={{
        paper: classes.paper,
      }}
      className={className}
      open={open}
      variant="persistent"
    >
      <List>
        {items.map(({ hideAfterClick, icon, name, onClick }, index) => {
          const handleClick = (): void => {
            if (onClick) {
              dispatch(onClick())

              if (hideAfterClick) {
                onClose()
              }
            }
          }

          return (
            <ListItem
              key={name}
              button
              onClick={handleClick}
            >
              {icon && <ListItemIcon>{icon}</ListItemIcon>}
              <ListItemText primary={name} />
            </ListItem>
          );
        })}
      </List>
      <Divider />
      <Toolbar className={classes.toolbar}>
        <IconButton onClick={onClose}>
          <ChevronRightIcon />
        </IconButton>
      </Toolbar>
    </Drawer>
  );
}

export default memo(Menu);
