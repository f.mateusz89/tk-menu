import React from 'react';
import { storiesOf } from '@storybook/react';
import { select, text } from '@storybook/addon-knobs';
import { action } from '../storybook/actions';
import { closeMenu, openMenu } from '../actions';
import { filledState, withStore } from '../storybook/decorators/withStore';
import Menu from './Menu';
import withActions from '../storybook/decorators/withActions';
storiesOf('Menu', module)
    .addParameters({ jest: ['Menu'] })
    .addDecorator(withActions(closeMenu, openMenu))
    .addDecorator(withStore(filledState, { menu: { open: true } }))
    .addWithJSX('default', function () {
    var anchor = select('anchor', ['bottom', 'left', 'right', 'top'], 'right');
    var item1 = text('Important', 'Important');
    var item2 = text('Today', 'Today');
    var item3 = text('Settings', 'Settings');
    var items = [
        { name: item1, onClick: function () { return ({ type: "ACTION: " + item1 }); } },
        { name: item2, onClick: function () { return ({ type: "ACTION: " + item2 }); } },
        { name: item3, onClick: function () { return ({ type: "ACTION: " + item3 }); } },
    ];
    return React.createElement(Menu, { anchor: anchor, items: items, onClose: action('onClose') });
});
