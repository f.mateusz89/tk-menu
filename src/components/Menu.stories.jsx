import React from 'react';
import { storiesOf } from '@storybook/react';
import { select, text } from '@storybook/addon-knobs';

import { action } from '../storybook/actions';
import { closeMenu, openMenu } from '../actions';
import { filledState, withStore } from '../storybook/decorators/withStore';
import Menu from './Menu';
import withActions from '../storybook/decorators/withActions';

storiesOf('Menu', module)
  .addParameters({ jest: ['Menu'] })
  .addDecorator(withActions(closeMenu, openMenu))
  .addDecorator(withStore(filledState, { menu: { open: true } }))
  .addWithJSX('default', () => {
    const anchor = select(
      'anchor',
      ['bottom', 'left', 'right', 'top'],
      'right',
    );
    const item1 = text('Important', 'Important');
    const item2 = text('Today', 'Today');
    const item3 = text('Settings', 'Settings');
    const items = [
      { name: item1, onClick: () => ({ type: `ACTION: ${item1}` }) },
      { name: item2, onClick: () => ({ type: `ACTION: ${item2}` }) },
      { name: item3, onClick: () => ({ type: `ACTION: ${item3}` }) },
    ];

    return <Menu anchor={anchor} items={items} onClose={action('onClose')} />;
  });
