import React from 'react';
import { OwnProps } from '../types';
declare function Menu({ anchor, className, items, }: OwnProps): React.ReactElement;
declare const _default: React.MemoExoticComponent<typeof Menu>;
export default _default;
