// import createType from 'utils/create-type';

// TODO: Create module + import
export default function createType(name: string, prefix?: string): string {
  return prefix ? `[${prefix}] ${name}` : name;
}

const PREFIX = 'menu';

export const CLOSE_MENU = createType('CLOSE_MENU', PREFIX);
export const OPEN_MENU = createType('OPEN_MENU', PREFIX);
