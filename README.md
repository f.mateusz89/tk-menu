# Table of contents

1. [How to include in project](#how-to-include-in-project)
1. [About](#about)
1. [Environment requirements](#environment-requirements)
1. [Develop](#develop)
1. [Production](#production)
1. [TODO](#todo)

## How to include in project

```js
yarn add tk-menu
// or
npm add tk-menu
```

1. Support by any chrome instance
   1. Open chrome instance (shortcut) properties and add following value `--remote-debugging-port=9222`
   1. Close all chrome instances
   1. In `.vscode/launch.json` set `runtimeExecutable` to the path to your favorite chrome instance (shortcut)
1. VSCode
1. Debug tab
1. Start debugging button
   - APP: [http://localhost:3000](http://localhost:3000)
   - Storybook: [http://localhost:6006](http://localhost:6006)

## About

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). Hosted on GitLab repository through pages per branch.
Boilerplate of React webpage with:

- Form
- i18n - translation management system
- MateriaUI - fancy UI
- Redux
- Router
- Sagas - to handle complex async / sync flow
- Snapshots
- Styled components

HTTPS is disabled, it can be enabled by changing flag `HTTPS` to `true` in `.env` file. With HTTPS enabled HRM is unable to update.

## Environment requirements

- Node version: v13.0.1
- npm version: 6.12.0

## Production

Variables available to modify:

- `process.env.REACT_APP_BACKEND_URL` - ULR to API / BACKEND
- `process.env.PUBLIC_URL` - change app root path

In most cases, it can be set in CI.

## Develop

I am using VSCode. Config files are in the repository.
After `yarn start` and `yarn test` (in another tab) then:

1. Open VSCode
1. DEBBUGER tab
1. Click the start button.

By default it will start:

1. APP [http://localhost:3000](http://localhost:3000)
1. Storybook [http://localhost:6006](http://localhost:6006)

## TODO

1. Move Containers to Hooks as per docs it gave great performance boost
1. Consider to move Containers to Hooks
1. Move `jest.mock('react-redux'` to `setupTests` once containers will be removed
1. Set tests coverage in CI
1. Create generic type of `children` prop

Low priority:

1. Enhance snackbar with small progress indicator
1. Allow to fetch data in background
1. Making a Progressive Web App: This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app
1. Add redux-batch - to handle batch actions
1. Customize MUI colors
1. Concurrent mode https://reactjs.org/docs/concurrent-mode-adoption.html
1. Set Jest to run directly from VSCode
1. Support H2 with BE
