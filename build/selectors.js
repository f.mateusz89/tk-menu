import { createSelector } from 'reselect';
function rootSelector(state) {
    return state.menu;
}
export var open = createSelector(rootSelector, function (root) { return root.open; });
