import { combineReducers } from 'redux';
import { CLOSE_MENU, OPEN_MENU } from './constants';
function open(state, _a) {
    if (state === void 0) { state = false; }
    var type = _a.type;
    switch (type) {
        case CLOSE_MENU:
            return false;
        case OPEN_MENU:
            return true;
        default:
            return state;
    }
}
export default combineReducers({
    open: open,
});
