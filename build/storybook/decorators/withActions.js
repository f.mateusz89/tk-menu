import React from 'react';
import { connect } from 'react-redux';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
var useStyles = makeStyles(function (theme) {
    return createStyles({
        buttons: {
            '& > *': {
                margin: theme.spacing(1),
            },
        },
    });
});
var withActions = function () {
    var actions = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        actions[_i] = arguments[_i];
    }
    return function (story) {
        function WithActions(_a) {
            var dispatch = _a.dispatch;
            var classes = useStyles();
            return (React.createElement(React.Fragment, null,
                React.createElement(Box, { className: classes.buttons, p: 1 }, actions.map(function (action, index) { return (React.createElement(Button, { key: index, color: "primary", onClick: function () { return dispatch(action()); }, variant: "contained" }, action.name)); })),
                story()));
        }
        var WithDispatch = connect(null, function (dispatch) { return ({ dispatch: dispatch }); })(WithActions);
        return React.createElement(WithDispatch, null);
    };
};
export default withActions;
