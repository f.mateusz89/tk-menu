import filledState from "./filledState";
export function withStore(...states: any[]): (story: any) => JSX.Element;
export { filledState };
