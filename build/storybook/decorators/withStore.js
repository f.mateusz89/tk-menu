var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
import React from 'react';
import { combineReducers, createStore } from 'redux';
import { Provider } from 'react-redux';
import merge from 'lodash/merge';
import { action } from '../actions';
import filledState from './filledState';
import menu from '../../reducer';
var store = createStore(combineReducers({
    menu: menu,
}));
var withStore = function () {
    var states = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        states[_i] = arguments[_i];
    }
    return function (story) {
        return (React.createElement(Provider, { store: __assign(__assign({}, store), { dispatch: function (dispatchAction) {
                    action("[DISPATCH]" + dispatchAction.type)(dispatchAction);
                    return store.dispatch(dispatchAction);
                }, getState: function () {
                    var state = store.getState();
                    return merge.apply(void 0, __spreadArrays([{}], states, [state]));
                } }) }, story()));
    };
};
export { filledState, withStore };
