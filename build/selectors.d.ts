import { State } from './types';
export declare const open: import("reselect").OutputSelector<State, boolean, (res: {
    open: boolean;
}) => boolean>;
