declare const _default: import("redux").Reducer<import("redux").CombinedState<{
    open: boolean;
}>, import("redux").AnyAction>;
export default _default;
