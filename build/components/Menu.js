import React, { memo } from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Toolbar from '@material-ui/core/Toolbar';
import { closeMenu } from '../actions';
import * as selectors from '../selectors';
var useStyles = makeStyles(function () {
    return createStyles({
        paper: {
            display: 'flex',
            justifyContent: 'flex-end',
        },
        toolbar: {
            justifyContent: 'flex-end',
        },
    });
});
function Menu(_a) {
    var _b = _a.anchor, anchor = _b === void 0 ? 'right' : _b, className = _a.className, items = _a.items;
    var classes = useStyles();
    var dispatch = useDispatch();
    var open = useSelector(selectors.open);
    var onClose = function () {
        dispatch(closeMenu());
    };
    return (React.createElement(Drawer, { anchor: anchor, classes: {
            paper: classes.paper,
        }, className: className, open: open, variant: "persistent" },
        React.createElement(List, null, items.map(function (_a, index) {
            var hideAfterClick = _a.hideAfterClick, icon = _a.icon, name = _a.name, onClick = _a.onClick;
            var handleClick = function () {
                if (onClick) {
                    dispatch(onClick());
                    if (hideAfterClick) {
                        onClose();
                    }
                }
            };
            return (React.createElement(ListItem, { key: name, button: true, onClick: handleClick },
                icon && React.createElement(ListItemIcon, null, icon),
                React.createElement(ListItemText, { primary: name })));
        })),
        React.createElement(Divider, null),
        React.createElement(Toolbar, { className: classes.toolbar },
            React.createElement(IconButton, { onClick: onClose },
                React.createElement(ChevronRightIcon, null)))));
}
export default memo(Menu);
