import { addDecorator, configure, setAddon } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
// import { withTests } from '@storybook/addon-jest';
import JSXAddon from 'storybook-addon-jsx';

// import results from '../.jest-test-results.json';

setAddon(JSXAddon);
addDecorator(withKnobs);
// addDecorator(withTests({ results }));

const req = require.context('../build', true, /\.stories.(js|jsx|ts|tsx)$/);

const getFixedName = (name) =>
  name
    .toLowerCase()
    .split('/')
    .slice(-1);

function loadStories() {
  req
    .keys()
    .sort((fileNameA, fileNameB) =>
      getFixedName(fileNameA) < getFixedName(fileNameB) ? -1 : 1,
    )
    .forEach((filename) => req(filename));
}

configure(loadStories, module);
